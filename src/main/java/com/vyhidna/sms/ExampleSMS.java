package com.vyhidna.sms;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {

  // Find your Account Sid and Token at twilio.com/user/account
  public static final String ACCOUNT_SID = "AC4492193fedecaed3788949e785caa3c3";
  public static final String AUTH_TOKEN = "7f2b2a62b4c73037a31e92836488e1c0";

  public static void send(String str) {
    Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
    Message message = Message
        .creator(new PhoneNumber("+380663190540"),
            /*my phone number*/
            new PhoneNumber("+12563644258"), str).create(); /*attached to me number*/
  }
}
