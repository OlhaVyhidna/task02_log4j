package com.vyhidna.view;

import com.vyhidna.Main;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Scanner;
import com.vyhidna.models.Dwelling;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DwellingView {

  private Scanner scannerForNumbers = new Scanner(System.in);
  private Scanner scannerForStrings = new Scanner(System.in);
  private static Logger logger =  LogManager.getLogger(Main.class);

  public int mainMenu() {

    System.out.println(MenuOptions.WELCOME);
    System.out.println(MenuOptions.CHOOSE_ACTION);
    System.out.println(MenuOptions.STARTUP_OPTIONS);

    return scannerForNumbers.nextInt();
  }

  public String doesChooseAParticularClass() {
    System.out.println(MenuOptions.SEARCH_IN_PARTICULAR_CLASS);
    return scannerForStrings.nextLine();
  }

  public int defineTheClass() {
    System.out.println(MenuOptions.CATEGORY_DEFINING);
    System.out.println(MenuOptions.CLASS_DEFINING);
    return scannerForNumbers.nextInt();
  }

  public String askAboutContinue(){
    System.out.println(MenuOptions.ASK_ABOUT_CONTINUE);
    return scannerForStrings.nextLine();
  }

  public int numberOfParameters() {
    System.out.println(MenuOptions.NUMBER_OF_PARAMETERS);
    return scannerForNumbers.nextInt();
  }

  public int chooseTheParameter(Field[] declaredFields) {
    System.out.println(MenuOptions.CHOOSE_THE_PARAMETER);
    for (int i = 0; i < declaredFields.length; i++) {
      Field declaredField = declaredFields[i];
      System.out.println(i + 1 + " - " + declaredField.getName());
    }
    return scannerForNumbers.nextInt();
  }

  public int getMaxValue() {
    System.out.println(MenuOptions.MAX_VALUE);
    return scannerForNumbers.nextInt();
  }

  public int getMinValue() {
    System.out.println(MenuOptions.MIN_VALUE);
    return scannerForNumbers.nextInt();
  }

  public void displayItems(List<? extends Dwelling> dwellings) {
    for (Dwelling dwelling : dwellings) {
      System.out.println(dwelling);
    }
  }


}
