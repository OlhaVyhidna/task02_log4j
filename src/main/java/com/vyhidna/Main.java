package com.vyhidna;

import com.vyhidna.controllers.DwellingController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
private static Logger logger1 =  LogManager.getLogger(Main.class);

  public static void main(String[] args) {
    logger1.trace("Hello this is a trace message");
    logger1.debug("Hello this is a debug message");
    logger1.info("Hello this is a info message");
    logger1.warn("Hello this is a warn message");
    logger1.error("Hello this is an error message");
    logger1.fatal("Hello this is fatal message");
    DwellingController dwellingController = new DwellingController();
    dwellingController.startApplication();
  }

}
