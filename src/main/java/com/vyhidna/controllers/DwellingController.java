package com.vyhidna.controllers;

import java.lang.reflect.Field;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import com.vyhidna.models.Dwelling;
import com.vyhidna.models.Flat;
import com.vyhidna.models.HousingInAHighRiceBuilding;
import com.vyhidna.models.Mansion;
import com.vyhidna.models.Penthouse;
import com.vyhidna.models.PrivateHouse;
import com.vyhidna.models.Realtor;
import com.vyhidna.models.Townhouse;
import com.vyhidna.view.DwellingView;

public class DwellingController {

  private final Predicate<Dwelling> riceBuildingPredicate = o ->
      o instanceof Flat || o instanceof Penthouse;

  private final Predicate<Dwelling> townhousePredicate = o ->
      (o instanceof Townhouse) || o instanceof Mansion;

  private Realtor realtor;
  private DwellingView dwellingView;

  public DwellingController() {
    this.dwellingView = new DwellingView();
    this.realtor = new Realtor();
  }

  public void startApplication() {
    int mainMenuResult = this.dwellingView.mainMenu();
    switch (mainMenuResult) {
      case 1: {
        dwellingView.displayItems(realtor.getDwellings());
        break;
      }
      case 2: {
        Class<? extends Dwelling> className = Dwelling.class;
        if (userAnswer(dwellingView.doesChooseAParticularClass())) {
          className = defineTheClass(dwellingView.defineTheClass());
          userDecisionAboutContinuing();
        }
        Field[] fields = className.getDeclaredFields();
        int fieldNumber = dwellingView.chooseTheParameter(fields);
        List<? extends Dwelling> dwellings = sortByField(fieldNumber, fields, className);
        dwellingView.displayItems(dwellings);
        userDecisionAboutContinuing();
        break;
      }
      case 3: {
        Class<? extends Dwelling> className = defineTheClass(dwellingView.defineTheClass());
        Field[] fields = className.getDeclaredFields();
        int numberOfParameters = dwellingView.numberOfParameters();
        List<? extends Dwelling> dwellings = startFiltering(numberOfParameters, fields,
            className);
        System.out.println(dwellings);
        dwellingView.displayItems(dwellings);
        userDecisionAboutContinuing();
        break;

      }
    }
  }

  private boolean userAnswer(String s) {
    return "Y".equals(s);
  }

  private void userDecisionAboutContinuing(){
    String doContinue = dwellingView.askAboutContinue();
    if (userAnswer(doContinue)){
      startApplication();
    }
  }

  private List<? extends Dwelling> startFiltering(int numberOfParameters, Field[] fields,
      Class<? extends Dwelling> className) {
    List<? extends Dwelling> dwellings = this.realtor.getDwellings();
    for (int i = 0; i < numberOfParameters; i++) {
      int fieldNumber = dwellingView.chooseTheParameter(fields);
      int maxValue = dwellingView.getMaxValue();
      int minValue = dwellingView.getMinValue();
      dwellings = filterDwellings(fieldNumber, fields, maxValue, minValue, className, dwellings);
    }
    return dwellings;
  }

  private List<? extends Dwelling> filterDwellings(int fieldNumber, Field[] fields,
      int maxValue,
      int minValue, Class<? extends Dwelling> className,
      List<? extends Dwelling> currentDwellings) {
    List<? extends Dwelling> dwellings = extractDwellingByClass(className);
    String fieldName = fields[fieldNumber - 1].getName();

    switch (fieldName) {
      case "numberOfFloorsInDwelling": {
        return dwellings.stream()
            .filter(o -> o.getNumberOfFloorsInDwelling() >= minValue &&
                o.getNumberOfFloorsInDwelling() <= maxValue)
            .collect(Collectors.toList());
      }
      case "areaOfDwelling": {
        return dwellings.stream()
            .filter(o -> o.getAreaOfDwelling() >= minValue &&
                o.getAreaOfDwelling() <= maxValue)
            .collect(Collectors.toList());
      }
      case "numberOfRooms": {
        return dwellings.stream()
            .filter(o -> o.getNumberOfRooms() >= minValue &&
                o.getNumberOfRooms() <= maxValue).collect(Collectors.toList());
      }
      case "distanceToKindergartenInMeters": {
        return dwellings.stream()
            .filter(o -> o.getDistanceToKindergartenInMeters() >= minValue &&
                o.getDistanceToKindergartenInMeters() <= maxValue)
            .collect(Collectors.toList());
      }
      case "distanceToSchoolInMeters": {
        return dwellings.stream()
            .filter(o -> ((Dwelling) o).getDistanceToSchoolInMeters() >= minValue &&
                ((Dwelling) o).getDistanceToSchoolInMeters() <= maxValue)
            .collect(Collectors.toList());
      }
      case "distanceToPlaygroundInMeters": {
        return dwellings.stream()
            .filter(o -> ((Dwelling) o).getDistanceToPlaygroundInMeters() >= minValue
                && ((Dwelling) o).getDistanceToPlaygroundInMeters() <= maxValue)
            .collect(Collectors.toList());
      }
      case "floorNumber": {
        return dwellings.stream()
            .filter(o -> ((HousingInAHighRiceBuilding) o).getFloorNumber() >= maxValue &&
                ((HousingInAHighRiceBuilding) o).getFloorNumber() <= maxValue)
            .collect(Collectors.toList());
      }
      case "adjoiningTerritory": {
        return dwellings.stream()
            .filter(o -> ((PrivateHouse) o).getAdjoiningTerritory() >= minValue &&
                ((PrivateHouse) o).getAdjoiningTerritory() <= maxValue)
            .collect(Collectors.toList());
      }
      case "numberOfSections": {
        return dwellings.stream()
            .filter(o -> ((Townhouse) o).getNumberOfSections() >= minValue &&
                ((Townhouse) o).getNumberOfSections() <= maxValue).collect(Collectors.toList());
      }
      case "flatType": {
        return dwellings.stream()
            .sorted((o1, o2) -> ((Flat) o1).getFlatType().compareTo(((Flat) o2).getFlatType()))
            .collect(Collectors.toList());

      }
    }
    return dwellings;
  }


  private List<? extends Dwelling> sortByField(int fieldNumber, Field[] fields,
      Class<? extends Dwelling> className) {
    List<? extends Dwelling> dwellings = extractDwellingByClass(className);
    String fieldName = fields[fieldNumber - 1].getName();
    switch (fieldName) {
      case "numberOfFloorsInDwelling": {
        return dwellings.stream()
            .sorted(Comparator.comparingInt(Dwelling::getNumberOfFloorsInDwelling))
            .collect(Collectors.toList());
      }
      case "areaOfDwelling": {
        return dwellings.stream()
            .sorted(Comparator.comparingInt(Dwelling::getAreaOfDwelling))
            .collect(Collectors.toList());
      }
      case "numberOfRooms": {
        return dwellings.stream()
            .sorted(Comparator.comparingInt(Dwelling::getNumberOfRooms))
            .collect(Collectors.toList());
      }
      case "distanceToKindergartenInMeters": {
        return dwellings.stream()
            .sorted(Comparator.comparingInt(Dwelling::getDistanceToKindergartenInMeters))
            .collect(Collectors.toList());
      }
      case "distanceToSchoolInMeters": {
        return dwellings.stream()
            .sorted(Comparator.comparingInt(Dwelling::getDistanceToSchoolInMeters))
            .collect(Collectors.toList());
      }
      case "distanceToPlaygroundInMeters": {
        return dwellings.stream()
            .sorted(Comparator.comparingInt(Dwelling::getDistanceToPlaygroundInMeters))
            .collect(Collectors.toList());
      }
      case "floorNumber": {
        return dwellings.stream()
            .map(HousingInAHighRiceBuilding.class::cast)
            .sorted(Comparator.comparingInt(HousingInAHighRiceBuilding::getFloorNumber))
            .collect(Collectors.toList());
      }
      case "adjoiningTerritory": {
        return dwellings.stream()
            .map(PrivateHouse.class::cast)
            .sorted(Comparator.comparingInt(PrivateHouse::getAdjoiningTerritory))
            .collect(Collectors.toList());
      }
      case "numberOfSections": {
        return dwellings.stream()
            .map(Townhouse.class::cast)
            .sorted((Comparator.comparingInt(Townhouse::getAdjoiningTerritory)))
            .collect(Collectors.toList());
      }
      case "flatType": {
        return dwellings.stream()
            .sorted((o1, o2) -> ((Flat) o1).getFlatType().compareTo(((Flat) o2).getFlatType()))
            .collect(Collectors.toList());

      }
    }
    return null;
  }

  private Class<? extends Dwelling> defineTheClass(int enteredNumber) {
    Class<? extends Dwelling> className = null;
    switch (enteredNumber) {
      case 1: {
        className = PrivateHouse.class;
        break;
      }
      case 2: {
        className = HousingInAHighRiceBuilding.class;
        break;
      }
      case 3: {
        className = Mansion.class;
        break;
      }
      case 4: {
        className = Townhouse.class;
        break;
      }
      case 5: {
        className = Flat.class;
        break;
      }
      case 6: {
        className = Penthouse.class;
      }
    }
    return className;
  }

  private List<? extends Dwelling> extractDwellingByClass(Class<? extends Dwelling> className) {
    List<? extends Dwelling> dwellings;
    if (className.isAssignableFrom(PrivateHouse.class)) {
      dwellings = this.realtor.getDwellings().stream().filter(
          townhousePredicate).collect(Collectors.toList());
    } else if (className.isAssignableFrom(HousingInAHighRiceBuilding.class)) {
      dwellings = this.realtor.getDwellings().stream().filter(
          riceBuildingPredicate).collect(Collectors.toList());
    } else if (className.isAssignableFrom(Dwelling.class)) {
      dwellings = this.realtor.getDwellings();
    } else {
      dwellings = this.realtor.getDwellings().stream().filter(o ->
          o.getClass().isAssignableFrom(className)).collect(Collectors.toList());
    }
    return dwellings;
  }


}
