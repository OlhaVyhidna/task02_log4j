package com.vyhidna.models;

public abstract class HousingInAHighRiceBuilding extends Dwelling {

  private int numberOfFloorsInDwelling;
  private int areaOfDwelling;
  private int numberOfRooms;
  private int distanceToKindergartenInMeters;
  private int distanceToSchoolInMeters;
  private int distanceToPlaygroundInMeters;
  private int floorNumber;

  public HousingInAHighRiceBuilding(int numberOfFloorsInDwelling, int areaOfDwelling,
      int numberOfRooms, int distanceToKindergartenInMeters, int distanceToSchoolInMeters,
      int distanceToPlaygroundInMeters, int floorNumber) {
    super(numberOfFloorsInDwelling, areaOfDwelling, numberOfRooms, distanceToKindergartenInMeters,
        distanceToSchoolInMeters, distanceToPlaygroundInMeters);
    this.floorNumber = floorNumber;
  }

  public int getFloorNumber() {
    return floorNumber;
  }

  public void setFloorNumber(int floorNumber) {
    this.floorNumber = floorNumber;
  }
}
