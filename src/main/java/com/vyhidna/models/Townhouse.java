package com.vyhidna.models;

public class Townhouse extends PrivateHouse {

  private int numberOfFloorsInDwelling;
  private int areaOfDwelling;
  private int numberOfRooms;
  private int distanceToKindergartenInMeters;
  private int distanceToSchoolInMeters;
  private int distanceToPlaygroundInMeters;
  private int adjoiningTerritory;
  private int numberOfSections;

  public Townhouse(int numberOfFloorsInDwelling, int areaOfDwelling, int numberOfRooms,
      int distanceToKindergartenInMeters, int distanceToSchoolInMeters,
      int distanceToPlaygroundInMeters, int adjoiningTerritory,
      int numberOfSections) {
    super(numberOfFloorsInDwelling, areaOfDwelling, numberOfRooms, distanceToKindergartenInMeters,
        distanceToSchoolInMeters, distanceToPlaygroundInMeters, adjoiningTerritory);
    this.numberOfSections = numberOfSections;
  }

  public int getNumberOfSections() {
    return numberOfSections;
  }

  public void setNumberOfSections(int numberOfSections) {
    this.numberOfSections = numberOfSections;
  }

  @Override
  public String toString() {
    return "Townhouse{" +
        "numberOfFloorsInDwelling=" + getNumberOfFloorsInDwelling() +
        ", areaOfDwelling=" + getAreaOfDwelling() +
        ", numberOfRooms=" + getNumberOfRooms() +
        ", distanceToKindergartenInMeters=" + getDistanceToKindergartenInMeters() +
        ", distanceToSchoolInMeters=" + getDistanceToSchoolInMeters() +
        ", distanceToPlaygroundInMeters=" + getDistanceToPlaygroundInMeters() +
        ", adjoiningTerritory=" + getAdjoiningTerritory() +
        ", numberOfSections=" + getNumberOfSections() +
        '}';
  }
}
