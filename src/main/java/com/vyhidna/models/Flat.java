package com.vyhidna.models;

public class Flat extends HousingInAHighRiceBuilding {

  final static int numberOfFloors = 1;
  private int numberOfFloorsInDwelling;
  private int areaOfDwelling;
  private int numberOfRooms;
  private int distanceToKindergartenInMeters;
  private int distanceToSchoolInMeters;
  private int distanceToPlaygroundInMeters;
  private int floorNumber;
  private FlatType flatType;


  public Flat(int areaOfDwelling, int numberOfRooms,
      int distanceToKindergartenInMeters, int distanceToSchoolInMeters,
      int distanceToPlaygroundInMeters, int floorNumber,
      FlatType flatType) {
    super(numberOfFloors, areaOfDwelling, numberOfRooms, distanceToKindergartenInMeters,
        distanceToSchoolInMeters, distanceToPlaygroundInMeters, floorNumber);
    this.flatType = flatType;
  }

  public FlatType getFlatType() {
    return flatType;
  }

  public void setFlatType(FlatType flatType) {
    this.flatType = flatType;
  }


  @Override
  public String toString() {
    return "Flat{" +
        "numberOfFloorsInDwelling=" + getNumberOfFloorsInDwelling() +
        ", areaOfDwelling=" + getAreaOfDwelling() +
        ", numberOfRooms=" + getNumberOfRooms() +
        ", distanceToKindergartenInMeters=" + getDistanceToKindergartenInMeters() +
        ", distanceToSchoolInMeters=" + getDistanceToSchoolInMeters() +
        ", distanceToPlaygroundInMeters=" + getDistanceToPlaygroundInMeters() +
        ", floorNumber=" + getFloorNumber() +
        ", flatType=" + getFlatType() +
        '}';
  }
}
