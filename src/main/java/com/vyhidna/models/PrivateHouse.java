package com.vyhidna.models;

public abstract class PrivateHouse extends Dwelling {

  private int numberOfFloorsInDwelling;
  private int areaOfDwelling;
  private int numberOfRooms;
  private int distanceToKindergartenInMeters;
  private int distanceToSchoolInMeters;
  private int distanceToPlaygroundInMeters;
  private int adjoiningTerritory;

  public PrivateHouse(int numberOfFloorsInDwelling, int areaOfDwelling, int numberOfRooms,
      int distanceToKindergartenInMeters, int distanceToSchoolInMeters,
      int distanceToPlaygroundInMeters, int adjoiningTerritory) {
    super(numberOfFloorsInDwelling, areaOfDwelling, numberOfRooms, distanceToKindergartenInMeters,
        distanceToSchoolInMeters, distanceToPlaygroundInMeters);
    this.adjoiningTerritory = adjoiningTerritory;
  }

  public int getAdjoiningTerritory() {
    return adjoiningTerritory;
  }

  public void setAdjoiningTerritory(int adjoiningTerritory) {
    this.adjoiningTerritory = adjoiningTerritory;
  }
}
