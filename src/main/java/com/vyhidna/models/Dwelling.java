package com.vyhidna.models;

public abstract class Dwelling {

  private int numberOfFloorsInDwelling;
  private int areaOfDwelling;
  private int numberOfRooms;
  private int distanceToKindergartenInMeters;
  private int distanceToSchoolInMeters;
  private int distanceToPlaygroundInMeters;

  public Dwelling(int numberOfFloorsInDwelling, int areaOfDwelling, int numberOfRooms,
      int distanceToKindergartenInMeters, int distanceToSchoolInMeters,
      int distanceToPlaygroundInMeters) {
    this.numberOfFloorsInDwelling = numberOfFloorsInDwelling;
    this.areaOfDwelling = areaOfDwelling;
    this.numberOfRooms = numberOfRooms;
    this.distanceToKindergartenInMeters = distanceToKindergartenInMeters;
    this.distanceToSchoolInMeters = distanceToSchoolInMeters;
    this.distanceToPlaygroundInMeters = distanceToPlaygroundInMeters;
  }

  public int getNumberOfFloorsInDwelling() {
    return numberOfFloorsInDwelling;
  }

  public void setNumberOfFloorsInDwelling(int numberOfFloorsInDwelling) {
    this.numberOfFloorsInDwelling = numberOfFloorsInDwelling;
  }

  public int getAreaOfDwelling() {
    return areaOfDwelling;
  }

  public void setAreaOfDwelling(int areaOfDwelling) {
    this.areaOfDwelling = areaOfDwelling;
  }

  public int getNumberOfRooms() {
    return numberOfRooms;
  }

  public void setNumberOfRooms(int numberOfRooms) {
    this.numberOfRooms = numberOfRooms;
  }

  public int getDistanceToKindergartenInMeters() {
    return distanceToKindergartenInMeters;
  }

  public void setDistanceToKindergartenInMeters(int distanceToKindergartenInMeters) {
    this.distanceToKindergartenInMeters = distanceToKindergartenInMeters;
  }

  public int getDistanceToSchoolInMeters() {
    return distanceToSchoolInMeters;
  }

  public void setDistanceToSchoolInMeters(int distanceToSchoolInMeters) {
    this.distanceToSchoolInMeters = distanceToSchoolInMeters;
  }

  public int getDistanceToPlaygroundInMeters() {
    return distanceToPlaygroundInMeters;
  }

  public void setDistanceToPlaygroundInMeters(int distanceToPlaygroundInMeters) {
    this.distanceToPlaygroundInMeters = distanceToPlaygroundInMeters;
  }
}
